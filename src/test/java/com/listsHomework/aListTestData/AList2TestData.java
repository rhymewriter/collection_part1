package com.listsHomework.aListTestData;

import com.listsHomework.implementations.AList2;

public class AList2TestData {

    public static final Integer[] ORIGINAL_LIST_1 = new Integer[] {null, null, null, null, null, null, null, null, null,
            null};

    public static final Integer[] ORIGINAL_LIST_2 = new Integer[] {13, 34, 21, null, null, null, null, null, null, null};

    public static final AList2<Integer> ORIGINAL_A_LIST_1 = new AList2<>();

    public static final AList2<Integer> ORIGINAL_A_LIST_2 = new AList2<>(ORIGINAL_LIST_2);

    public static final Integer[] ADD_LIST_1 = new Integer[] {15, null, null, null, null, null, null, null, null,
            null};
    public static final Integer[] ADD_LIST_2 = new Integer[] {13, 34, 21, 20, null, null, null, null, null, null};

    public static final AList2<Integer> ADD_A_LIST_1 = new AList2<>(ADD_LIST_1);

    public static final AList2<Integer> ADD_A_LIST_2 = new AList2<>(ADD_LIST_2);

    public static final Integer[] ADD_BY_INDEX_LIST_1 = new Integer[] {10, null, null, null, null, null, null, null, null,
            null};
    public static final Integer[] ADD_BY_INDEX_LIST_2 = new Integer[] {13, 34, 25, 21, null, null, null, null, null, null};

    public static final AList2<Integer> ADD_BY_INDEX_A_LIST_1 = new AList2<>(ADD_BY_INDEX_LIST_1);

    public static final AList2<Integer> ADD_BY_INDEX_A_LIST_2 = new AList2<>(ADD_BY_INDEX_LIST_2);

    public static final Integer[] REMOVE_LIST_1 = new Integer[] {34, 21, null, null, null, null, null, null, null, null};

    public static final Integer[] REMOVE_LIST_2 = new Integer[] {13, 34, null, null, null, null, null, null, null, null};

    public static final AList2<Integer> REMOVE_A_LIST_1 = new AList2<>(REMOVE_LIST_1);

    public static final AList2<Integer> REMOVE_A_LIST_2 = new AList2<>(REMOVE_LIST_2);

    public static final Integer[] REMOVE_BY_INDEX_LIST_1 = new Integer[] {34, 21, null, null, null, null, null, null, null,
            null};

    public static final Integer[] REMOVE_BY_INDEX_LIST_2 = new Integer[] {13, 34, null, null, null, null, null, null, null,
            null};

    public static final AList2<Integer> REMOVE_BY_INDEX_A_LIST_1 = new AList2<>(REMOVE_BY_INDEX_LIST_1);

    public static final AList2<Integer> REMOVE_BY_INDEX_A_LIST_2 = new AList2<>(REMOVE_BY_INDEX_LIST_2);

    public static final Integer[] SET_LIST_1 = new Integer[] {13, 22, 21, null, null, null, null, null, null, null};

    public static final Integer[] SET_LIST_2 = new Integer[] {13, 34, 11, null, null, null, null, null, null, null};

    public static final AList2<Integer> SET_A_LIST_1 = new AList2<>(SET_LIST_1);

    public static final AList2<Integer> SET_A_LIST_2 = new AList2<>(SET_LIST_2);

    public static final Integer[] REMOVE_ALL_LIST_1 = new Integer[] {34, null, null, null, null, null, null, null, null,
            null};

    public static final Integer[] REMOVE_ALL_LIST_2 = new Integer[] {null, null, null, null, null, null, null, null, null,
            null};

    public static final AList2<Integer> REMOVE_ALL_A_LIST_1 = new AList2<>(REMOVE_ALL_LIST_1);

    public static final AList2<Integer> REMOVE_ALL_A_LIST_2 = new AList2<>(REMOVE_ALL_LIST_2);

}
