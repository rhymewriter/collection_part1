package com.listsHomework.aListTestData;

import com.listsHomework.implementations.AList1;

public class AList1TestData {
    public static final int[] ORIGINAL_LIST_1 = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    public static final int[] ORIGINAL_LIST_2 = new int[] {13, 34, 21, 0, 0, 0, 0, 0, 0, 0};

    public static final int[] ORIGINAL_LIST_2_FOR_A_LIST = new int[] {13, 34, 21};

    public static final AList1 ORIGINAL_A_LIST_1 = new AList1();

    public static final AList1 ORIGINAL_A_LIST_2 = new AList1(ORIGINAL_LIST_2_FOR_A_LIST);

    public static final int[] ADD_LIST_1 = new int[] {15};
    public static final int[] ADD_LIST_2 = new int[] {13, 34, 21, 20};

    public static final AList1 ADD_A_LIST_1 = new AList1(ADD_LIST_1);

    public static final AList1 ADD_A_LIST_2 = new AList1(ADD_LIST_2);

    public static final int[] ADD_BY_INDEX_LIST_1 = new int[] {10};
    public static final int[] ADD_BY_INDEX_LIST_2 = new int[] {13, 34, 25, 21};

    public static final AList1 ADD_BY_INDEX_A_LIST_1 = new AList1(ADD_BY_INDEX_LIST_1);

    public static final AList1 ADD_BY_INDEX_A_LIST_2 = new AList1(ADD_BY_INDEX_LIST_2);

    public static final int[] REMOVE_LIST_1 = new int[] {34, 21};

    public static final int[] REMOVE_LIST_2 = new int[] {13, 34};

    public static final AList1 REMOVE_A_LIST_1 = new AList1(REMOVE_LIST_1);

    public static final AList1 REMOVE_A_LIST_2 = new AList1(REMOVE_LIST_2);

    public static final int[] REMOVE_BY_INDEX_LIST_1 = new int[] {34, 21};

    public static final int[] REMOVE_BY_INDEX_LIST_2 = new int[] {13, 34};

    public static final AList1 REMOVE_BY_INDEX_A_LIST_1 = new AList1(REMOVE_BY_INDEX_LIST_1);

    public static final AList1 REMOVE_BY_INDEX_A_LIST_2 = new AList1(REMOVE_BY_INDEX_LIST_2);

    public static final int[] SET_LIST_1 = new int[] {13, 22, 21};

    public static final int[] SET_LIST_2 = new int[] {13, 34, 11};

    public static final AList1 SET_A_LIST_1 = new AList1(SET_LIST_1);

    public static final AList1 SET_A_LIST_2 = new AList1(SET_LIST_2);

    public static final int[] REMOVE_ALL_LIST_1 = new int[] {34};

    public static final int[] REMOVE_ALL_LIST_2 = new int[] {};

    public static final AList1 REMOVE_ALL_A_LIST_1 = new AList1(REMOVE_ALL_LIST_1);

    public static final AList1 REMOVE_ALL_A_LIST_2 = new AList1(REMOVE_ALL_LIST_2);


}
