package com.listsHomework.implementations;

import static com.listsHomework.aListTestData.AList2TestData.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Field;
import java.util.Arrays;

public class AList2Test {
    AList2<Integer> cut = new AList2<>();

    static Arguments[] clearTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, ORIGINAL_LIST_1, 0, ORIGINAL_A_LIST_1),
                Arguments.arguments(10, ORIGINAL_LIST_2, 3, ORIGINAL_A_LIST_1)
        };
    }

    @ParameterizedTest
    @MethodSource("clearTestArgs")
    void clearTest(int reflectC, Object[] reflectA, int reflectS, AList2<Integer> expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.clear();

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] getTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, ORIGINAL_LIST_2, 3, 0, 13),
                Arguments.arguments(10, ORIGINAL_LIST_2, 3, 2, 21)
        };
    }

    @ParameterizedTest
    @MethodSource("getTestArgs")
    void getTest(int reflectC, Object[] reflectA, int reflectS, int index, int expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        int actual = cut.get(index);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] addTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_1, ORIGINAL_LIST_1.length), 0, 15, ADD_A_LIST_1),
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3, 20, ADD_A_LIST_2)
        };
    }

    @ParameterizedTest
    @MethodSource("addTestArgs")
    void addTest(int reflectC, Object[] reflectA, int reflectS, int input, AList2<Integer> expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.add(input);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] addByIndexTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_1, ORIGINAL_LIST_1.length), 0, 0, 10, ADD_BY_INDEX_A_LIST_1),
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3, 2, 25, ADD_BY_INDEX_A_LIST_2)
        };
    }

    @ParameterizedTest
    @MethodSource("addByIndexTestArgs")
    void addByIndexTest(int reflectC, Object[] reflectA, int reflectS, int index, int input, AList2<Integer> expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.add(index, input);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] removeTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3, 13, REMOVE_A_LIST_1),
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3, 21, REMOVE_A_LIST_2)
        };
    }

    @ParameterizedTest
    @MethodSource("removeTestArgs")
    void removeTest(int reflectC, Object[] reflectA, int reflectS, int number, AList2<Integer> expected)
            throws NoSuchFieldException, IllegalAccessException{
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.remove(number);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] removeByIndexTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3, 0, REMOVE_BY_INDEX_A_LIST_1),
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3, 2, REMOVE_BY_INDEX_A_LIST_2)
        };
    }

    @ParameterizedTest
    @MethodSource("removeByIndexTestArgs")
    void removeByIndexTest(int reflectC, Object[] reflectA, int reflectS, int index,  AList2<Integer> expected)
            throws NoSuchFieldException, IllegalAccessException{
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.removeByIndex(index);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] containsTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3, 15, false),
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3, 34, true)
        };
    }

    @ParameterizedTest
    @MethodSource("containsTestArgs")
    void containsTest(int reflectC, Object[] reflectA, int reflectS, int number, boolean expected)
            throws NoSuchFieldException, IllegalAccessException{
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        boolean actual = cut.contains(number);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] setTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3, 1, 22, SET_A_LIST_1),
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3, 2, 11, SET_A_LIST_2)
        };
    }

    @ParameterizedTest
    @MethodSource("setTestArgs")
    void setTest(int reflectC, Object[] reflectA, int reflectS, int index, int number, AList2<Integer> expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.set(index, number);

        Assertions.assertEquals(expected, cut);
    }

    static Arguments[] toArrayTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_1, ORIGINAL_LIST_1.length), 0,
                        (Object[]) new Integer[] {}),
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3,
                        (Object[])new Integer[] {13, 34, 21})
        };
    }

    @ParameterizedTest
    @MethodSource("toArrayTestArgs")
    void toArrayTest(int reflectC, Object[] reflectA, int reflectS, Object[] expected)
            throws IllegalAccessException, NoSuchFieldException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        Object[] actual = cut.toArray();

        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] removeAllTestArgs() {
        return new Arguments[] {
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3,
                        new Integer[] {13, 54, 12, 21, 69}, REMOVE_ALL_A_LIST_1),
                Arguments.arguments(10, Arrays.copyOf(ORIGINAL_LIST_2, ORIGINAL_LIST_2.length), 3,
                        new Integer[] {98, 13, 34, 21, 69}, REMOVE_ALL_A_LIST_2)
        };
    }

    @ParameterizedTest
    @MethodSource("removeAllTestArgs")
    void removeAllTest(int reflectC, Object[] reflectA, int reflectS, Integer[] input, AList2<Integer> expected)
            throws NoSuchFieldException, IllegalAccessException {
        Field fieldCapacity = cut.getClass().getDeclaredField("capacity");
        fieldCapacity.setAccessible(true);
        fieldCapacity.set(cut, reflectC);

        Field fieldArray = cut.getClass().getDeclaredField("array");
        fieldArray.setAccessible(true);
        fieldArray.set(cut, reflectA);

        Field fieldSize = cut.getClass().getDeclaredField("size");
        fieldSize.setAccessible(true);
        fieldSize.set(cut, reflectS);

        cut.removeAll(input);

        Assertions.assertEquals(expected, cut);
    }
}
